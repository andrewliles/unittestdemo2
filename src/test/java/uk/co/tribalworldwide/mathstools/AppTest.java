package uk.co.tribalworldwide.mathstools;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Test;

public class AppTest
{
    @Test
    public void fibonnaci0() {
    	assertThat(App.fibonnaci(0), is(0));
    }
    
    @Test
    public void fibonnaci1() {
    	assertThat(App.fibonnaci(1), is(1));
    }
    
    @Test
    public void fibonnaci2() {
    	assertThat(App.fibonnaci(2), is(1));
    }
    
    @Test
    public void fibonnaci3() {
    	assertThat(App.fibonnaci(3), is(2));
    }
    
    @Test
    public void fibonnaciMulti() {
    	int[] expected = {0,1,1,2,3,5,8,13,21,34,55};
        for(int i=0; i<expected.length; i++) {
            assertThat(App.fibonnaci(i), is(expected[i]));
        }
    }   
    
    @Test
    public void fibonnaci_neg1() {
    	assertThat(App.fibonnaci(-1), is(0));
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void fibonnaci_large() {
    	assertThat(App.fibonnaci(100), is(0));
    }
}
